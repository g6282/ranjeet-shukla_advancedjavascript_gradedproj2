let resumeIndex = 0;
let resumeDataArray = [];
let resumeDataArrayTemp = [];
const nextBtn = document.getElementById("nextBtn");
const prevBtn = document.getElementById("prevBtn");
const profileSearch = document.getElementById("profileSearch");
const resumeContainer = document.getElementById("resumeContainer");
const errorContainer = document.getElementById("errorContainer");

function readJsonFile() {
  return fetch("./data.json")
    .then((response) => response.json())
    .then((res) => res && res.resume);
}

function fetchResumeRow(resume, index) {
  validateButton(resume?.length, index);
  if (resume.length > 0) {
    return resume[index];
  } else {
    return [];
  }
}

function nextResumeIndex() {
  resumeIndex++;
}
function prevResumeIndex() {
  resumeIndex--;
}

function validateButton(totalLength, currentIndex) {
  prevBtn.style.visibility = "visible";
  nextBtn.style.visibility = "visible";
  if (currentIndex === 0) {
    prevBtn.style.visibility = "hidden";
  }
  if (totalLength === 0) {
    prevBtn.style.visibility = "hidden";
    nextBtn.style.visibility = "hidden";
  }
  if (currentIndex >= totalLength - 1) {
    nextBtn.style.visibility = "hidden";
  }
}

nextBtn.addEventListener("click", (e) => {
  nextResumeIndex();
  const resumeDataRow = fetchResumeRow(resumeDataArray, resumeIndex);
  paintResume(resumeDataRow);
});

prevBtn.addEventListener("click", (e) => {
  prevResumeIndex();
  const resumeDataRow = fetchResumeRow(resumeDataArray, resumeIndex);
  paintResume(resumeDataRow);
});

// Initial load JSON data
window.onload = async () => {
  resumeDataArray = resumeDataArrayTemp = await readJsonFile();
  const resumeDataRow = await fetchResumeRow(resumeDataArray, resumeIndex);
  paintResume(resumeDataRow);
};

// filter search
profileSearch.addEventListener("keyup", (e) => {
  const filteredResumeArray = resumeDataArrayTemp?.filter((data) => {
    const jobRole = data?.basics?.AppliedFor?.toLowerCase();
    return jobRole.includes(e.target.value);
  });
  if (filteredResumeArray.length > 0) {
    resumeDataArray = filteredResumeArray;
    resumeIndex = 0;
    const resumeDataRow = fetchResumeRow(resumeDataArray, resumeIndex);
    paintResume(resumeDataRow);
    resumeContainer.style.display = "block";
    errorContainer.style.display = "none";
  } else {
    resumeContainer.style.display = "none";
    errorContainer.style.display = "flex";
  }
});

/**
 *
 * @param {*} data
 * This function will manipulate the dom
 *
 */
function paintResume(data) {
  const applicantName = document.getElementById("applicantName");
  const applicantAppliedFor = document.getElementById("applicantAppliedFor");
  const mobileNo = document.getElementById("mobileNo");
  const emailId = document.getElementById("emailId");
  const socialProfile = document.getElementById("socialProfile");
  const skills = document.getElementById("skills");
  const hobbies = document.getElementById("hobbies");
  const workExperience = document.getElementById("workExperience");
  const projectSummary = document.getElementById("projectSummary");
  const education = document.getElementById("education");
  const internship = document.getElementById("internship");
  const achievements = document.getElementById("achievements");

  // header
  applicantName.innerText = data?.basics?.name;
  applicantAppliedFor.innerText = "Applied For : " + data?.basics?.AppliedFor;

  // left side
  mobileNo.innerText = data?.basics?.phone;
  emailId.innerText = data?.basics?.email;
  socialProfile.innerHTML = `<a href="${data?.basics?.profiles?.url}">${data?.basics?.profiles?.network}</a>`;
  const skillsArray = data?.skills?.keywords;
  skills.innerHTML = skillsArray
    .map((skillKeyworkd) => `<span>${skillKeyworkd}</span>`)
    .join("");
  const hobbiesArray = data?.interests?.hobbies;
  hobbies.innerHTML = hobbiesArray
    .map((hobbiesKeyworkd) => `<span>${hobbiesKeyworkd}</span>`)
    .join("");

  // Work Experience section
  let workHtml = "";
  const workKeys = Object.keys(data?.work);
  workKeys.forEach((key) => {
    const workValues = data?.work[key];
    workHtml += `<div class="sub-section-heading">${key} : <span class="sub-section-text">${workValues}</span></div>`;
  });
  workExperience.innerHTML = workHtml;

  // Projects section
  projectSummary.innerHTML = `${data?.projects?.name} : <span class="sub-section-text"
  >${data?.projects?.description}</span`;

  // Education section
  let educationHtml = "";
  const educationKeys = Object.keys(data?.education);
  educationKeys.forEach((level) => {
    const eduInnerValues = Object.values(data?.education[level]).join(", ");
    educationHtml += `<li class="sub-section-heading">${level} : <span class="sub-section-text">${eduInnerValues}</span></li>`;
  });
  education.innerHTML = educationHtml;

  // Internship section
  let internshipHtml = "";
  const internshipKeys = Object.keys(data?.Internship);
  internshipKeys.forEach((key) => {
    const internshipValues = data?.Internship[key];
    internshipHtml += `<li class="sub-section-heading">${key} : <span class="sub-section-text">${internshipValues}</span></li>`;
  });
  internship.innerHTML = internshipHtml;

  //Achievements section
  let achievementsHtml = "";
  const achievementsSummary = data?.achievements?.Summary;
  achievementsHtml = achievementsSummary
    .map(
      (summary) =>
        `<li class="sub-section-heading"><span class="sub-section-text">${summary}</span></li>`
    )
    .join("");
  achievements.innerHTML = achievementsHtml;
}
