(function () {
  // dom elements
  const usernameEle = document.getElementById("username");
  const passwordEle = document.getElementById("password");
  const loginButton = document.getElementById("login");
  const warningMsg = document.getElementById("warningMsg");

  loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    const inputUsername = usernameEle.value;
    const inputPassword = passwordEle.value;

    const { username, password } = getLoginCredsFromLocalStoreg();
    if (username === inputUsername && password === inputPassword) {
      window.location.replace("resume/resume.html");
    } else {
      warningMsg.innerHTML = "Invalid username/password";
    }
  });

  // setting default username and password
  setLoginCredsInLocalStoreg("ranjeet", "123456");

  function setLoginCredsInLocalStoreg(username = "", password = "") {
    if (localStorage) {
      localStorage.setItem("username", username);
      localStorage.setItem("password", password);
    }
  }

  function getLoginCredsFromLocalStoreg() {
    let username = "";
    let password = "";
    if (localStorage) {
      username = localStorage.getItem("username");
      password = localStorage.getItem("password");
    }
    return {
      username: username,
      password: password,
    };
  }
})();
